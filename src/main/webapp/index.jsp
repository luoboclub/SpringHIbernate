<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script type="text/javascript" src="http://www.jq-school.com/js/jquery-1.7.2.min.js"></script>
</head>
<body>
	<form action="${pageContext.request.contextPath}/Test/Name" method="post">
		用户名:<input type="text" name="name" id="username" onchange="refer()" /><span id="user"></span><br>
		密&nbsp;&nbsp;&nbsp;码:<input type="password" name="pwd" /><br> 
		<input type="submit" value="提交" />
	</form>
	<script type="text/javascript">
		function refer() {
			var name = $("#username").val();
			var url = "${pageContext.request.contextPath}/Test/Action";
			$.ajax({
				url : url,
				type : "post",
				data : {
					"Name" : name
				},
				dataType : "json",
				success : function(data) {
					if (data.success) {
						$("#user").html("不可用!");
					} else {
						$("#user").html("可用!");
					}
				}
			});
		}
	</script>
</body>
</html>
