/**
 * 
 */
package com.my.web.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author pc-Haitao
 * @ClassName:IBaseDao
 * @Version 版本
 * @Modifiedby 修改人
 * @Copyright 公司名称
 * @date 2016年9月8日上午11:20:51
 */
public interface IBaseDao<T> {
	public Serializable save(T entity);

	public T get(String hql);

	public T get(Class<T> c, Serializable id);

	public T get(String hql, Map<String, Object> params);

	public List<T> find(String hql, Map<String, Object> params);

	public void delete(T entity);

	public void update(T entity);


}
