/**
 * 
 */
package com.my.web.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.my.web.model.Usertest;



/**
 * @author pc-Haitao
 * @ClassName:IUserService
 * @Version 版本
 * @Modifiedby 修改人
 * @Copyright 公司名称
 * @date 2016年9月8日上午11:37:22
 */
public interface IUserService {
	public Serializable save(Usertest entity);

	public Usertest get(String hql);

	public Usertest get(Class<Usertest> c, Serializable id);

	public Usertest get(String hql, Map<String, Object> params);

	public List<Usertest> find(String hql, Map<String, Object> params);

	public void delete(Usertest entity);

	public void update(Usertest entity);
	public boolean verifyByName(String name);
}
