/**
 * 
 */
package com.my.web.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.my.web.dao.IBaseDao;
import com.my.web.model.Usertest;
import com.my.web.service.IUserService;

/**
 * @author pc-Haitao
 * @ClassName:UserServiceImpl
 * @Version 版本
 * @Modifiedby 修改人
 * @Copyright 公司名称
 * @date 2016年9月8日上午11:42:22
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
	@Autowired
	private IBaseDao<Usertest> iBaseDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#save(com.my.web.model.Usertest)
	 */
	@Override
	public Serializable save(Usertest entity) {

		return iBaseDao.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#get(java.lang.String)
	 */
	@Override
	public Usertest get(String hql) {
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#get(java.lang.Class, java.io.Serializable)
	 */
	@Override
	public Usertest get(Class<Usertest> c, Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#get(java.lang.String, java.util.Map)
	 */
	@Override
	public Usertest get(String hql, Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#find(java.lang.String, java.util.Map)
	 */
	@Override
	public List<Usertest> find(String hql, Map<String, Object> params) {
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#delete(com.my.web.model.Usertest)
	 */
	@Override
	public void delete(Usertest entity) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.my.web.service.IUserService#update(com.my.web.model.Usertest)
	 */
	@Override
	public void update(Usertest entity) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.my.web.service.IUserService#verifyByName(java.lang.String)
	 */
	@Override
	public boolean verifyByName(String name) {
		String hql = "FROM Usertest WHERE name=:name";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		List<Usertest> li = iBaseDao.find(hql, map);
		if (li.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

}
