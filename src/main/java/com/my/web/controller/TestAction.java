/**
 * 
 */
package com.my.web.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.my.web.model.Usertest;
import com.my.web.service.IUserService;
import com.my.web.util.MD5Util;

/**
 * @author pc-Haitao
 * @ClassName:TestAction
 * @Version 版本
 * @Modifiedby 修改人
 * @Copyright 公司名称
 * @date 2016年8月25日上午9:12:43
 */
@RestController
@RequestMapping("/Test")
public class TestAction {
	@Autowired
	private IUserService iUserService;//注入接口

	@RequestMapping(value = "/Action", method = { RequestMethod.POST, RequestMethod.GET })
	public Map<String, Boolean> test(HttpServletRequest request) {
		String s = request.getParameter("Name");
		boolean b = iUserService.verifyByName(s);
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		map.put("success", b);
		return map;
	}

	@RequestMapping(value = "/Name", method = { RequestMethod.POST, RequestMethod.GET })
	public Map<String, Object> save(String name, String pwd) {
		System.out.println(name + "==" + pwd);
		Usertest user = new Usertest();
		user.setId(UUID.randomUUID().toString());// 使用uuid生成的32位id
		user.setName(name);
		user.setPassword(MD5Util.encode2hex(pwd));// 使用md5加密密码 
		iUserService.save(user);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("user", user);
		return map;
	}
}
